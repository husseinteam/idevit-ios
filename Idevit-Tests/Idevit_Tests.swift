//
//  Idevit_Tests.swift
//  Idevit-Tests
//
//  Created by Hüseyin Sönmez on 08/02/2017.
//  Copyright © 2017 Hüseyin Sönmez. All rights reserved.
//

import XCTest
@testable import Idevit

class Idevit_Tests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testParseProductTree() {
        let expect = expectation(description: "Products will be retrieved from remote url and get parsed")
        let prodeng = IdevitEngine()
        prodeng.parseProductTree { productTree in
            XCTAssert(productTree.rootCategories.count > 0, "categories aren't much enough")
            for ctg in productTree.rootCategories {
                XCTAssert(ctg.id != 0, "category: \(ctg.name) has no id")
                for cctg in ctg.children {
                    XCTAssert(cctg.id != 0, "child category: \(cctg.name) has no id")
                }
            }
            expect.fulfill()
        }
        waitForExpectations(timeout: 3600) { error in
            if let error = error {
                XCTFail("waitForExpectations errored: \(error)")
            }
        }
    }

}
