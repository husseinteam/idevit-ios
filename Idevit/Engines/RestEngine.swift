//
//  RestEngine.swift
//  İdevit Seramik Ürün Kataloğu
//
//  Created by Hüseyin Sönmez on 08/02/2017.
//  Copyright © 2017 Hüseyin Sönmez. All rights reserved.
//

import Foundation
import UIKit

class RestEngine {
    
    private var baseUrl: String
    init(baseUrl: String) {
        self.baseUrl = String(describing: baseUrl.characters.last) == "/" ? baseUrl.substring(to: baseUrl.index(before: baseUrl.endIndex)) : baseUrl
    }
    
    private var cache: [String: Data?] = [:]
    
    func request<T>(to route: String, using httpMethod: HttpMethods, generator: @escaping ([String: Any]) -> T, failback: @escaping (Error?) -> Void, callback: @escaping ([T]) -> Void) {
        guard let url = URL(string: "\(self.baseUrl)/\(route)") else {
            print("Error: cannot parse URL")
            return
        }
        var request = URLRequest(url: url)
        request.httpMethod = httpMethod.description()
        
        switch httpMethod {
            case .POST(let data):
                do {
                    let json = try JSONSerialization.data(withJSONObject: data, options: [])
                    request.httpBody = json
                } catch {
                    print("Error: cannot create JSON from todo")
                    return
                }
                break
            case .PUT(let data):
                do {
                    let json = try JSONSerialization.data(withJSONObject: data, options: [])
                    request.httpBody = json
                } catch {
                    print("Error: cannot create JSON from todo")
                    return
                }
                break
            default:
                break
        }
        let handle: (Data?, URLResponse?, Error?) -> Void = { (data, response, error) in
            // check for any errors
            guard error == nil else {
                print("error calling GET on /\(route)")
                print(error!)
                failback(error)
                return
            }
            // make sure we got data
            guard let responseData = data else {
                print("Error: did not receive data")
                failback(error)
                return
            }
            self.cache[route] = responseData
            // parse the result as JSON, since that's what the API provides
            do {
                var models = [T]()
                if let json = try JSONSerialization.jsonObject(with: responseData, options: [])
                    as? [String: Any] {
                    models.append(generator(json))
                } else if let jsonArray = try JSONSerialization.jsonObject(with: responseData, options: [])
                    as? [[String: Any]] {
                    for json in jsonArray {
                        models.append(generator(json))
                    }
                } else {
                    print("error parsing data: \(responseData) as array or element of [String: Any]")
                    failback(error)
                }
                callback(models)
            } catch  {
                print("error trying to convert data to JSON, error: \(error)")
                failback(error)
            }
        }
        if self.cache.keys.contains(route) {
            handle(self.cache[route]!, nil, nil)
        } else {
            let session = URLSession.shared
            let task = session.dataTask(with: request) { (data, response, error) in
                handle(data, response, error)
            }
            task.resume()
        }
    }
    
}

enum HttpMethods {
    case GET
    case POST(data: [String: Any])
    case PUT(data: [String: Any])
    case DELETE
    
    func description() -> String {
        switch self {
            case .GET: return "GET"
            case .POST: return "POST"
            case .PUT: return "PUT"
            case .DELETE: return "DELETE"
        }
    }
}
