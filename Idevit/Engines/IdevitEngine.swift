//
//  ProductsEngine.swift
//  İdevit Seramik Ürün Kataloğu
//
//  Created by Hüseyin Sönmez on 08/02/2017.
//  Copyright © 2017 Hüseyin Sönmez. All rights reserved.
//

import Foundation
import UIKit

class IdevitEngine {
    
    private let resteng: RestEngine!
    var noConnectionHandler: ((Error?) -> Void) = { err in }
    
    init() {
        self.resteng = RestEngine(baseUrl: "http://idevit.com.tr/servis.php")
    }
    
    func parseOnePagePdf(callback: @escaping (Data?) -> Void) {
        self.resteng.request(to: "", using: HttpMethods.GET, generator: { (data) -> URL? in
            let pdfUrl = URL(string: data["imaj_katalog_pdf"] as? String ?? "")
            return pdfUrl
        }, failback: noConnectionHandler) { pdfUrlList in
            if let pdfUrl = pdfUrlList.first, let url = pdfUrl {
                url.prefetchData(
                    named: url.path.components(separatedBy: "/").last!.replacingOccurrences(of: ".pdf", with: ""),
                    withExtension: "pdf",
                    callback: { (pdfData) in
                    callback(pdfData)
                })
            }
            
        }
    }
    
    func parseContacts(callback: @escaping ([ContactModel]) -> Void) {
        self.resteng.request(to: "", using: HttpMethods.GET, generator: { (data) -> [ContactModel] in
            var contacts = [ContactModel]()
            if let iletisim = (data["iletisim"] as? [[String: Any]]) {
                for it in iletisim {
                    let model = ContactModel(data: it)
                    if it["tur"] as? String ?? "" != "konum" {
                        contacts.append(model)
                    }
                }
            }
            return contacts
        }, failback: noConnectionHandler) { contactsList in
            if let contacts = contactsList.first {
                callback(contacts)
            }
            
        }
    }
    func parseAssets(callback: @escaping (CatalogAssetsModel) -> Void) {
        self.resteng.request(to: "", using: HttpMethods.GET, generator: { (data) -> CatalogAssetsModel in
            let assets = CatalogAssetsModel()
            if let catalogLogoUrl = data["katalog_resim"] as? String,
                let catalogPdfUrl = data["katalog_pdf"] as? String {
                assets.catalogLogoUrl = URL(string: catalogLogoUrl)
                assets.catalogPdfUrl = URL(string: catalogPdfUrl)
            }
            var models = [PictureModel]()
            if let urunresimleri = (data["urun_detay_bilgileri"] as? [[String: Any]]) {
                for urunresim in urunresimleri {
                    let pmodel = PictureModel(data: urunresim)
                    if pmodel.lang == .tr{
                        models.append(pmodel)
                    }
                }
            }
            assets.pictures = models
            return assets
        }, failback: noConnectionHandler) { assetList in
            if let assets = assetList.first {
                callback(assets)
            }
            
        }
    }
    
    func parseAgents(callback: @escaping ([AgentModel]) -> Void) {
        self.resteng.request(to: "", using: HttpMethods.GET, generator: { (data) -> [AgentModel] in
            var agents = [AgentModel]()
            if let bayiler = (data["bayiler"] as? [[String: Any]]) {
                for bayi in bayiler {
                    agents.append(AgentModel(data: bayi))
                }
            }
            return agents
        }, failback: noConnectionHandler) { agentsList in
            if let agents = agentsList.first {
                callback(agents)
            }
            
        }
    }
    
    func parseProductTree(callback: @escaping (IdevitModel) -> Void) {
        self.resteng.request(to: "", using: HttpMethods.GET, generator: { (data) -> IdevitModel in
            let idevit = IdevitModel()
            if let products = (data["urunler"] as? [[String: Any]]),
                let tags = (data["etiketler"] as? [[String: Any]]),
                let sections = (data["katalog_bolumleri"] as? [[String: Any]]),
                let categories = (data["kategoriler"] as? [[String: Any]])?
                    .sorted(by: { (prev, next) -> Bool in
                        UIView.toInt(prev["order"]) < UIView.toInt(next["order"]) }) {
                var looper: (ProductCategoryModel, Int) -> Void = { parent, depth in }
                looper = { parent, d in
                    for child in categories.filter({ UIView.toInt($0["parent"]) == parent.id }) {
                        let childCategory = ProductCategoryModel(data: child, parentCategory: parent, depth: d + 1)
                        let productsOfChild = products.filter {
                            UIView.toIntArray($0["kategori_hiyerarsisi"]).contains(childCategory.id)
                        }
                        for prod in productsOfChild {
                            childCategory.appendProduct(ProductModel(data: prod))
                        }
                        parent.appendChild(childCategory)
                        looper(childCategory, d + 1)
                    }
                }
                for c in categories.filter({ UIView.toInt($0["parent"]) == 0 }) {
                    let rootCategory = ProductCategoryModel(data: c, parentCategory: nil, depth: 0)
                    if rootCategory.name.hasPrefix("Uncategorized") ||
                        rootCategory.language == LangEnum.en {
                        continue
                    }
                    let productsOfRoot = products.filter {
                        UIView.toIntArray($0["kategori_hiyerarsisi"]).contains(rootCategory.id)
                    }
                    for prod in productsOfRoot {
                        rootCategory.appendProduct(ProductModel(data: prod))
                    }
                    looper(rootCategory, 0)
                    idevit.appendCategory(rootCategory)
                }
                
                var looper2: (ProductTagModel, Int) -> Void = { parent, depth in }
                looper2 = { parent, d in
                    for child in tags.filter({ UIView.toInt($0["parent"]) == parent.id }) {
                        let childTag = ProductTagModel(data: child, parentTag: parent, depth: d + 1)
                        let productsOfChild = products.filter {
                            UIView.toIntArray($0["kategori_hiyerarsisi"]).contains(childTag.id) ||
                                ($0["etiketler"] as? [String] ?? [""]).contains(childTag.name)
                        }
                        for prod in productsOfChild {
                            childTag.appendProduct(ProductModel(data: prod))
                        }
                        parent.appendChild(childTag)
                        looper2(childTag, d + 1)
                    }
                }
                for c in tags.filter({ UIView.toInt($0["parent"]) == 0 }) {
                    let rootTag = ProductTagModel(data: c, parentTag: nil, depth: 0)
                    if rootTag.name.hasPrefix("Uncategorized") ||
                        rootTag.language == LangEnum.en {
                        continue
                    }
                    let productsOfRoot = products.filter {
                        UIView.toIntArray($0["kategori_hiyerarsisi"]).contains(rootTag.id)
                    }
                    for prod in productsOfRoot {
                        rootTag.appendProduct(ProductModel(data: prod))
                    }
                    looper2(rootTag, 0)
                    idevit.appendTag(rootTag)
                }
                idevit.sortProducts()
                for s in sections {
                    let section = PdfSectionModel(data: s)
                    idevit.appendSection(section)
                }
            }
            return idevit
        }, failback: noConnectionHandler) { idevitList in
            if let idevit = idevitList.first {
                callback(idevit)
            }
            
        }
    }
    
}
