//
//  SeguedViewControllerProtocol.swift
//  İdevit Seramik Ürün Kataloğu
//
//  Created by Hüseyin Sönmez on 06/02/2017.
//  Copyright © 2017 Hüseyin Sönmez. All rights reserved.
//

import Foundation

protocol SeguedViewControllerProtocol {
    var destinationIdentifier: String { get }
}
