//
//  ProductViewControllerProtocol.swift
//  Idevit
//
//  Created by Lampiclobe on 02/03/2017.
//  Copyright © 2017 Hüseyin Sönmez. All rights reserved.
//

import Foundation

protocol ProductViewControllerProtocol {
    func apply(callback: @escaping () -> Void)
}
