//
//  PriceCatalogCell.swift
//  Idevit
//
//  Created by Hüseyin Sönmez on 16/02/2017.
//  Copyright © 2017 Hüseyin Sönmez. All rights reserved.
//

import UIKit

class PriceCatalogCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    // MARK - Outlets
    
    @IBOutlet weak var title: UILabel!

    // MARK - Locals
    var model: PdfSectionModel? = nil {
        didSet {
            if let m = self.model {
                self.title?.text = m.title
            }
        }
    }
    var selectedHandler: ((PriceCatalogCell, Bool) -> Void)? = nil
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        if (self.isSelected == selected) {
            return
        }
        super.setSelected(selected, animated: animated)
        selectedHandler?(self, selected)
    }
    
}
