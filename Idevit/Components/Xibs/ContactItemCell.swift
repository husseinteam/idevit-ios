//
//  ContactItemCell.swift
//  Idevit
//
//  Created by Lampiclobe on 27/02/2017.
//  Copyright © 2017 Hüseyin Sönmez. All rights reserved.
//

import UIKit

class ContactItemCell: UITableViewCell {
    
    @IBOutlet weak var number: UILabel!
    @IBOutlet weak var type: UILabel!
    @IBOutlet weak var icon: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    var model: ContactModel? = nil {
        didSet {
            self.updateOutlets()
        }
    }
    
    // MARK: - Methods
    private func updateOutlets() {
        DispatchQueue.main.async {
            if let m = self.model {
                self.type?.text = m.subcontent!
                switch m.type {
                case .phone:
                    self.icon?.applyImage(#imageLiteral(resourceName: "phone"))
                    self.addPhoneCallHandler()
                    let underlined = NSAttributedString(
                        string: m.content ?? "", attributes:
                        [NSUnderlineStyleAttributeName: NSUnderlineStyle.styleSingle.rawValue])
                    self.number?.attributedText = underlined
                    break
                case .whatsapp:
                    self.icon?.applyImage(#imageLiteral(resourceName: "whatsapp"))
                    self.addWhatsappCallHandler()
                    break
                default:
                    self.icon?.image = nil
                    self.number?.text = m.content!
                    break
                }
                
            }
        }
    }
    
    private func addPhoneCallHandler() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.phoneCall(_:)))
        self.icon?.isUserInteractionEnabled = true
        self.icon?.gestureRecognizers?.removeAll()
        self.icon?.addGestureRecognizer(tap)
    }
    
    private func addWhatsappCallHandler() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.whatsappCall(_:)))
        self.icon?.isUserInteractionEnabled = true
        self.icon?.gestureRecognizers?.removeAll()
        self.icon?.addGestureRecognizer(tap)
    }
    
    func phoneCall(_ sender: UITapGestureRecognizer) {
        if let m = self.model {
            URL(string: "tel://\(m.content!.replacingOccurrences(of: " ", with: ""))")?.tryOpen()
        }
    }
    
    func whatsappCall(_ sender: UITapGestureRecognizer) {
        if let m = self.model {
            URL(string: "whatsapp://send?abid=\(m.content!.replacingOccurrences(of: " ", with: ""))")?.tryOpen()
        }
    }
}
