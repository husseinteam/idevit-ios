//
//  ProductCell.swift
//  İdevit Seramik Ürün Kataloğu
//
//  Created by Hüseyin Sönmez on 06/02/2017.
//  Copyright © 2017 Hüseyin Sönmez. All rights reserved.
//

import UIKit

class ProductCell: UICollectionViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        let tap = UITapGestureRecognizer(
            target: self,
            action: #selector(self.toDetailTapped(_:)))
        self.isUserInteractionEnabled = true
        self.addGestureRecognizer(tap)
    }
    
    // MARK: - Outlets
    
    @IBOutlet weak var productCodeLabel: UILabel!
    @IBOutlet weak var mainImageView: UIImageView!
    @IBOutlet var featureImages: [UIImageView]!
    @IBOutlet weak var activator: UIActivityIndicatorView!
    
    // MARK: - Actions
    func toDetailTapped(_ sender: UITapGestureRecognizer) {
        self.handler?(self.model)
    }
    
    
    var model: ProductModel! {
        didSet {
            self.updateOutlets()
        }
    }
    
    private func updateOutlets() {
        if (self.model != nil) {
            self.mainImageView.image = nil
            self.featureImages.forEach { fimg in
                fimg.image = nil
            }
            self.activator.startAnimating()
            self.productCodeLabel.text = self.model.productCode
            self.model.fetchMainImage { (img, featureds) in
                self.mainImageView.setImageFading(img) {
                    let limit = min(self.featureImages.count,
                                    featureds.count)
                    for i in 0 ..< limit {
                        self.featureImages[i].setImageFading(featureds[i])
                    }
                    self.activator.stopAnimating()
                }
            }
        }
    }
    
    // MARK: - Locals
    var handler: ((_ model: ProductModel) -> Void)? = nil
    
}
