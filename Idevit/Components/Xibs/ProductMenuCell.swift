//
//  ProductMenuCell.swift
//  Idevit
//
//  Created by Hüseyin Sönmez on 13/02/2017.
//  Copyright © 2017 Hüseyin Sönmez. All rights reserved.
//

import UIKit

class ProductMenuCell: UITableViewCell {

    @IBOutlet weak var indicator: UIImageView!
    @IBOutlet weak var productItemLabel: UILabel!
    @IBOutlet weak var leftMargin: NSLayoutConstraint!
    
    var model: ProductCategoryModel? = nil {
        didSet {
            DispatchQueue.main.async {
                if let cmodel = self.model {
                    self.productItemLabel?.text = cmodel.name!
                    self.leftMargin?.constant = CGFloat(cmodel.depth * 14)
                    self.indicator?.isHidden = cmodel.children.count == 0
                }
            }
        }
    }
    var tagmodel: ProductTagModel? = nil {
        didSet {
            DispatchQueue.main.async {
                if let tmodel = self.tagmodel {
                    self.productItemLabel?.text = tmodel.name!
                    self.leftMargin?.constant = CGFloat(tmodel.depth * 14)
                    self.indicator?.isHidden = tmodel.children.count == 0
                }
            }
        }
    }

    var infoTappedHandler : ((ProductMenuCell) -> Void)? 
    @IBAction func infoTapped(_ sender: UIButton) {
        infoTappedHandler?(self)
    }
    
}
