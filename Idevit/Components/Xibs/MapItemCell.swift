//
//  MapItemCell.swift
//  Idevit
//
//  Created by Lampiclobe on 28/02/2017.
//  Copyright © 2017 Hüseyin Sönmez. All rights reserved.
//

import UIKit
import MapKit

class MapItemCell: UITableViewCell {

    @IBOutlet weak var map: MKMapView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    // MARK: - Model
    var model: ContactModel? = nil {
        didSet {
            self.updateOutlets()
        }
    }
 
    // MARK: - Methods
    func updateOutlets() {
        DispatchQueue.main.async {
            if let m = self.model {
                switch m.type {
                case .googlemaps(let latitude, let longitude):
                    
                    let coordinate =
                        CLLocationCoordinate2D(latitude: latitude,
                                               longitude: longitude)
                    let span = MKCoordinateSpanMake(0.07, 0.07)
                    let region = MKCoordinateRegionMake(coordinate, span)
                    let annotation = MKPointAnnotation()
                    annotation.coordinate = coordinate
                    self.map.setRegion(region, animated: true)
                    self.map.addAnnotation(annotation)
                    break
                default:
                    break
                }
            }
        }
    }
    
}
