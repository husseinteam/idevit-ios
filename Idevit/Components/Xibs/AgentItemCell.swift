//
//  AgentItemCell.swift
//  Idevit
//
//  Created by Lampiclobe on 21/02/2017.
//  Copyright © 2017 Hüseyin Sönmez. All rights reserved.
//

import UIKit

class AgentItemCell: UICollectionViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    // MARK: - Outlets
    @IBOutlet weak var district: UILabel!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var phone: UILabel!
    @IBOutlet weak var fax: UILabel!
    
    
    // MARK: - Model
    var model: AgentModel? = nil {
        didSet {
            self.updateOutlets()
        }
    }
    
    // MARK: - Methods
    private func updateOutlets() {
        if let m = self.model {
            DispatchQueue.main.async {
                self.district?.text = m.district
                self.title?.text = m.title
                self.address?.text = m.address
                self.phone?.text = m.contact.slice(from: "TEL:", to: "\n")
                self.fax?.text = m.contact.slice(from: "FAX:", to: "\n")
            }
        }
    }
    
    // MARK: - Overrides
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch: UITouch in touches {
            let location = touch.location(in: self)
            if self.phone.frame.contains(location) {
                URL(string: "tel://\(self.phone!.text!.replacingOccurrences(of: " ", with: ""))")?.tryOpen()
            }
        }
    }
}
