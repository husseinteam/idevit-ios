//
//  BorderedView.swift
//  İdevit Seramik Ürün Kataloğu
//
//  Created by Hüseyin Sönmez on 07/02/2017.
//  Copyright © 2017 Hüseyin Sönmez. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
class BorderedView: UIView {
    
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
            layer.masksToBounds = cornerRadius > 0
        }
    }
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    @IBInspectable var borderColor: UIColor? {
        didSet {
            layer.borderColor = borderColor?.cgColor
        }
    }
    
}
