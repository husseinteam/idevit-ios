//
//  ParserExtensions.swift
//  Idevit
//
//  Created by Hüseyin Sönmez on 08/02/2017.
//  Copyright © 2017 Hüseyin Sönmez. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    
    static func toIntArray(_ obj: Any?) -> [Int] {
        var arr = [Int]()
        if let objArr = obj as? [Any] {
            for a in objArr {
                if let s = a as? String {
                    if let i = Int(s) {
                        arr.append(i)
                    }
                }
            }
        }
        return arr
    }
    
    static func toInt(_ obj: Any?) -> Int {
        if let s = obj as? String {
            if s != "" {
                if let i = Int(s) {
                    return i
                }
            }
        }
        return 0
    }
    
}

extension String {
    
    func slice(from: String, to: String) -> String? {
        
        return (range(of: from)?.upperBound).flatMap { substringFrom in
            (range(of: to, range: substringFrom..<endIndex)?.lowerBound).map { substringTo in
                substring(with: substringFrom..<substringTo)
            }
        }
    }
}
