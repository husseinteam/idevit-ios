//
//  EnumerableExtensions.swift
//  Idevit
//
//  Created by Lampiclobe on 23/02/2017.
//  Copyright © 2017 Hüseyin Sönmez. All rights reserved.
//

import Foundation


extension Array {
    
    func any(_ tryback: (Element) -> Bool) -> Bool {
        for it in self {
            if tryback(it) {
                return true
            }
        }
        return false
    }
    func first(of tryback: (Element) -> Bool) -> Element? {
        for it in self {
            if tryback(it) {
                return it
            }
        }
        return nil
    }
    
    func countOf(_ extractor: @escaping ((Element) -> Bool)) -> Int {
        return self.filter({ extractor($0) }).count
    }
    
    func uniquefied<TKey>(by extractor: @escaping (Element) -> TKey) -> Array where TKey: Equatable {
        var result = [Element]()
        for it in self {
            if self.countOf({ extractor($0) == extractor(it) }) == 1 {
                result.append(it)
            }
        }
        return result
    }
    
    func sliced(from: Int, to: Int) -> Array {
        guard to <= self.count else {
            return self
        }
        guard from < to else {
            return self
        }
        var sliced = [Element]()
        for i in from..<(to > self.count ? self.count : to) {
            sliced.append(self[i])
        }
        return sliced
    }
}
