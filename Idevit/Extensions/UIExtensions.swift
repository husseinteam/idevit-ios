//
//  UIExtensions.swift
//  İdevit Seramik Ürün Kataloğu
//
//  Created by Hüseyin Sönmez on 03/02/2017.
//  Copyright © 2017 Hüseyin Sönmez. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    func screenSize() -> CGSize {
        let ssize = UIScreen.main.bounds
        return CGSize(width: ssize.width, height: ssize.height)
    }
    func forceNav() {
        DispatchQueue.main.async {
            self.navigationController?.setToolbarHidden(false, animated: false)
            self.navigationController?.isToolbarHidden = true
        }
    }
}

extension UIView {
    private func processBackground(image: UIImage, contextMode: UIViewContentMode = .scaleAspectFit) {
        // setup the UIImageView
        DispatchQueue.main.async {
            let bgView = UIImageView(frame: UIScreen.main.bounds)
            bgView.image = image
            bgView.contentMode = contextMode
            bgView.translatesAutoresizingMaskIntoConstraints = false
            self.backgroundColor = self.backgroundColor ?? UIColor.white
            
            self.addSubview(bgView)
            self.sendSubview(toBack: bgView)
            
            // adding NSLayoutConstraints
            let leadingConstraint = NSLayoutConstraint(item: bgView, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1.0, constant: 0.0)
            let trailingConstraint = NSLayoutConstraint(item: bgView, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1.0, constant: 0.0)
            let topConstraint = NSLayoutConstraint(item: bgView, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1.0, constant: 0.0)
            let bottomConstraint = NSLayoutConstraint(item: bgView, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1.0, constant: 0.0)
            
            NSLayoutConstraint.activate([leadingConstraint, trailingConstraint, topConstraint, bottomConstraint])
        }
    }
    func addBackground(imageName: String, contextMode: UIViewContentMode = .scaleToFill) {
        processBackground(image: UIImage(named: imageName)!, contextMode: contextMode)
    }
    func addBackground(image: UIImage, contextMode: UIViewContentMode = .scaleToFill) {
        processBackground(image: image, contextMode: contextMode)
    }
}

extension URL {
    
    func tryOpen() {
        if UIApplication.shared.canOpenURL(self) {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(self, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(self)
            }
        }
    }
    
}

extension URL {
    
    func fetchData(callback: @escaping (Data?) -> Void) {
        URLSession.shared.dataTask(with: self) { (data, response, error) in
            if (error == nil) {
                if let d = data {
                    callback(d)
                } else {
                    callback(nil)
                }
            }
            else {
                callback(nil)
            }
            }.resume()
    }
    func prefetchData(named: String, withExtension: String, callback: @escaping (Data) -> Void) {
        if let docURL = (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)).last {
            let curYear = Calendar.current.dateComponents([.year], from: Date()).year!
            let fileURL = docURL.appendingPathComponent( "\(named)-\(curYear).\(withExtension)")
            if AppModel.clearFileCache {
                do {
                    try FileManager.default.removeItem(at: fileURL)
                } catch {
                    print("error: \(error) while deleting file at: \(fileURL.path)")
                }
            }
            if let data = FileManager.default.contents(atPath: fileURL.path) {
                callback(data)
            } else {
                self.fetchData { data in
                    if let dataNew = data {
                        do {
                            try dataNew.write(to: fileURL, options: Data.WritingOptions.atomic)
                            callback(dataNew)
                        } catch {
                            print("error while saving \(fileURL.path) to disk: \(error)")
                        }
                    }
                }
            }
        }
    }
    func fetchImage(callback: @escaping (UIImage) -> Void) {
        self.fetchData { data in
            if let d = data {
                callback(UIImage(data: d) ?? #imageLiteral(resourceName: "no-image"))
            } else {
                callback(#imageLiteral(resourceName: "no-image"))
            }
        }
    }
    
}

extension UIImageView {
    func setImageFading(_ image: UIImage?, applied: (() -> Void)? = nil) {
        self.applyImage(image, applied: applied)
        let transition = CATransition()
        transition.duration = 1.0
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionFade
        self.layer.add(transition, forKey: nil)
        self.setNeedsDisplay()
    }
    func applyImage(_ image: UIImage?, applied: (() -> Void)? = nil) {
        DispatchQueue.main.async {
            if let img = image {
//                UIGraphicsBeginImageContext(img.size)
//                let context = UIGraphicsGetCurrentContext()
//                context?.draw(img.cgImage!, in: CGRect(x: 0, y: 0, width: img.size.width, height: img.size.height))
//                UIGraphicsEndImageContext()
                self.image = img
                applied?()
            } else {
                applied?()
            }
        }
    }
    func fetchFrom(url: URL, contentMode mode: UIViewContentMode = .scaleAspectFit) {
        contentMode = mode
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() { () -> Void in
                self.setImageFading(image)
            }
        }.resume()
    }
    func fetchFrom(link: String, contentMode mode: UIViewContentMode = .scaleAspectFit) {
        guard let url = URL(string: link) else { return }
        fetchFrom(url: url, contentMode: mode)
    }
}

