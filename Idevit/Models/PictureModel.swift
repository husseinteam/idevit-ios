//
//  PictureModel.swift
//  Idevit
//
//  Created by Lampiclobe on 22/02/2017.
//  Copyright © 2017 Hüseyin Sönmez. All rights reserved.
//

import Foundation
import UIKit

class PictureModel {
    
    let lang: LangEnum
    let name: String!
    let frameUrl: URL?
    
    func fetchFrame(_ callback: @escaping (UIImage) -> Void) {
        if let url = self.frameUrl {
            url.fetchImage(callback: callback)
        } else {
            callback(#imageLiteral(resourceName: "no-image"))
        }
    }
    
    init(data: [String: Any]) {
        self.name = data["isim"] as? String ?? ""
        self.frameUrl = URL(string: "http://idevit.com.tr\(data["resim"] as? String ?? "")")
        self.lang = LangEnum(code: data["dil"] as? String ?? "")
    }
    
}
