//
//  AppModel.swift
//  Idevit
//
//  Created by Hüseyin Sönmez on 16/02/2017.
//  Copyright © 2017 Hüseyin Sönmez. All rights reserved.
//

import Foundation

class AppModel {
    static var idevit: IdevitModel? = nil
    static var assets: CatalogAssetsModel? = nil

    static let ideveng = IdevitEngine()
    
    static var clearFileCache: Bool = false
    
    static func set(idevit: IdevitModel) {
        AppModel.idevit = idevit
    }
}
