//
//  MaterialModel.swift
//  İdevit Seramik Ürün Kataloğu
//
//  Created by Hüseyin Sönmez on 06/02/2017.
//  Copyright © 2017 Hüseyin Sönmez. All rights reserved.
//

import Foundation
import UIKit

class MaterialModel {
    
    var materialCode: String!
    var description: String!
    
    init(materialCode: String, description: String) {
        self.materialCode = materialCode
        self.description = description
    }
}
