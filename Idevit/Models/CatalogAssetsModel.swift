//
//  CatalogAssetsModel.swift
//  Idevit
//
//  Created by Hüseyin Sönmez on 16/02/2017.
//  Copyright © 2017 Hüseyin Sönmez. All rights reserved.
//

import Foundation
import UIKit

class CatalogAssetsModel {
    
    var catalogLogoUrl: URL? = nil
    var catalogPdfUrl: URL? = nil
    var pictures: [PictureModel] = [PictureModel]()
}
