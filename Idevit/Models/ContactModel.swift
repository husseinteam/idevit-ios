//
//  ContactModel.swift
//  Idevit
//
//  Created by Lampiclobe on 27/02/2017.
//  Copyright © 2017 Hüseyin Sönmez. All rights reserved.
//

import Foundation

enum ContactType {
    case title
    case mail
    case address
    case gps
    case phone
    case googlemaps(latitude: Double, longitude: Double)
    case whatsapp
    
    init(_ data: [String: Any]) {
        let rawValue = data["tur"] as? String ?? ""
        switch rawValue {
        case "baslik":
            self = .title
            break
        case "mail":
            self = .mail
            break
        case "adres":
            self = .address
            break
        case "konum":
            self = .gps
            break
        case "telefon":
            self = .phone
            break
        case "whatsapp":
            self = .whatsapp
            break
        case "googlemaps":
            let ll = (data["icerik"] as? String ?? "")
                .components(separatedBy: ",").map { Double($0) }
            self = .googlemaps(latitude: ll[1] ?? 41.0427,
                               longitude: ll[0] ?? 29.0399)
            break
        default:
            self = .title
            break
        }
    }
}

class ContactModel {
    
    let type: ContactType
    let isTitle: Bool
    let isMap: Bool
    let content: String!
    let subcontent: String!
    init(data: [String: Any]) {
        self.type = ContactType(data)
        self.isTitle = data["tur"] as? String ?? "" == "baslik"
        self.isMap = data["tur"] as? String ?? "" == "googlemaps"
        if self.isTitle {
            self.content = data["baslik"] as? String ?? ""
            self.subcontent = "İletişim"
        } else {
            self.content = data["icerik"] as? String ?? ""
            self.subcontent = data["baslik"] as? String ?? ""
        }
    }
    
}
