//
//  AgentModel.swift
//  Idevit
//
//  Created by Lampiclobe on 21/02/2017.
//  Copyright © 2017 Hüseyin Sönmez. All rights reserved.
//

import Foundation

class AgentModel {
    
    let district: String!
    let title: String!
    let address: String!
    let contact: String!
    
    init(data: [String: Any]) {
        self.district = data["il"] as? String ?? ""
        self.title = data["satis_noktasi"] as? String ?? ""
        self.address = data["adres"] as? String ?? ""
        self.contact = data["telefon"] as? String ?? ""
    }
    
}
