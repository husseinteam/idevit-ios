//
//  ProductTagModel.swift
//  Idevit
//
//  Created by Lampiclobe on 02/03/2017.
//  Copyright © 2017 Hüseyin Sönmez. All rights reserved.
//

import Foundation
import UIKit

class ProductTagModel {

    let id: Int!
    let name: String!
    let slug: String!
    let order: Int!
    let count: Int!
    let depth: Int
    let language: LangEnum!
    var parentTag: ProductTagModel? = nil
    var children: [ProductTagModel]
    var products: [ProductModel]
    
    init(data: [String: Any], parentTag: ProductTagModel?, depth: Int) {
        self.id = UIView.toInt(data["term_id"])
        self.name = data["name"] as? String ?? ""
        self.slug = data["slug"] as? String ?? ""
        self.order = UIView.toInt(data["term_order"])
        self.count = UIView.toInt(data["count"])
        self.depth = depth
        self.language = LangEnum(code: data["dil_kodu"] as? String ?? "")
        self.parentTag = parentTag
        self.children = [ProductTagModel]()
        self.products = [ProductModel]()
    }

    func appendChild(_ child: ProductTagModel) {
        self.children.append(child);
    }
    
    func appendProduct(_ product: ProductModel) {
        self.products.append(product);
    }

}
