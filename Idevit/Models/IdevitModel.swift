//
//  ProductTree.swift
//  Idevit
//
//  Created by Hüseyin Sönmez on 09/02/2017.
//  Copyright © 2017 Hüseyin Sönmez. All rights reserved.
//

import Foundation
import UIKit

class IdevitModel {
    
    var rootCategories: [ProductCategoryModel]
    var rootTags: [ProductTagModel]
    var sections: [PdfSectionModel]

    init() {
        self.rootCategories = [ProductCategoryModel]()
        self.rootTags = [ProductTagModel]()
        self.sections = [PdfSectionModel]()
    }
    
    func appendCategory(_ category: ProductCategoryModel) {
        self.rootCategories.append(category)
    }
    
    func appendTag(_ tag: ProductTagModel) {
        self.rootTags.append(tag)
    }
    
    func appendSection(_ section: PdfSectionModel) {
        self.sections.append(section)
    }
    
    func sortProducts() {
        for rootCategory in self.rootCategories {
            self.sortProductsRecursively(parent: rootCategory)
        }
    }
    
    private func sortProductsRecursively(parent: ProductCategoryModel) {
        for childCategory in parent.children {
            childCategory.products = childCategory.products.sorted(by: { (prev, next) in prev.menuOrder < next.menuOrder })
            sortProductsRecursively(parent: childCategory)
        }
    }
    
}
