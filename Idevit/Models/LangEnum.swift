//
//  LangEnum.swift
//  Idevit
//
//  Created by Hüseyin Sönmez on 17/02/2017.
//  Copyright © 2017 Hüseyin Sönmez. All rights reserved.
//

import Foundation


enum LangEnum : Int {
    
    case tr = 1
    case en
}

extension LangEnum {
    init(code: String) {
        switch code {
        case "tr": self.init(rawValue: 1)!
        case "en": self.init(rawValue: 2)!
        default: self.init(rawValue: 1)!
        }
    }
}
