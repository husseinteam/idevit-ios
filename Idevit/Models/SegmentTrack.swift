//
//  SegmentTrack.swift
//  Idevit
//
//  Created by Hüseyin Sönmez on 13/02/2017.
//  Copyright © 2017 Hüseyin Sönmez. All rights reserved.
//

import Foundation
import UIKit

class SegmentTrack {
    
    var curtrack: ProductCategoryModel? = nil
    var curtag: ProductTagModel? = nil
    private let tree: IdevitModel!
    
    init(tree: IdevitModel) {
        self.tree = tree
    }
    
    func set(current track: ProductCategoryModel) {
        self.curtrack = track
    }
    
    func set(current tag: ProductTagModel) {
        self.curtag = tag
    }
    
    func rootCategories() -> [ProductCategoryModel] {
        return self.tree.rootCategories
    }
    
    func rootTags() -> [ProductTagModel] {
        return self.tree.rootTags
    }
    
    func currentBreadCrumbsText(forTag: Bool = false) -> String {
        var titles = [String]()
        if forTag {
            if let tag = self.curtag {
                titles.append("\(tag.name ?? "")")
                var parent = tag.parentTag
                while parent != nil {
                    titles.append("\(parent!.name!) / ")
                    parent = parent!.parentTag
                }
            }
        } else {
            if let category = self.curtrack {
                titles.append("\(category.name ?? "")")
                var parent = category.parentCategory
                while parent != nil {
                    titles.append("\(parent!.name!) / ")
                    parent = parent!.parentCategory
                }
            }
        }
        var text = ""
        for t in titles.reversed().map({ t in
            t.uppercased()
        }) {
            text += t
        }
        return text
    }
    
}
