//
//  PdfSectionModel.swift
//  Idevit
//
//  Created by Hüseyin Sönmez on 17/02/2017.
//  Copyright © 2017 Hüseyin Sönmez. All rights reserved.
//

import Foundation
import UIKit

class PdfSectionModel {
    
    let title: String!
    let page: Int!
    let lang: LangEnum!
    
    init(title: String, page: Int, lang: LangEnum) {
        self.title = title
        self.page = page
        self.lang = lang
    }
    
    convenience init(data: [String: Any]) {
        self.init(title: data["baslik"] as? String ?? "",
                  page: UIView.toInt(data["sayfa"]) + 1,
                  lang: LangEnum(code: data["dil"] as? String ?? ""))
    }
    
}
