//
//  ProductCategoryModel.swift
//  Idevit
//
//  Created by Hüseyin Sönmez on 08/02/2017.
//  Copyright © 2017 Hüseyin Sönmez. All rights reserved.
//

import Foundation
import UIKit

class ProductCategoryModel {

    let id: Int!
    let name: String!
    let slug: String!
    let order: Int!
    let count: Int!
    let depth: Int
    let language: LangEnum!
    var parentCategory: ProductCategoryModel? = nil
    var children: [ProductCategoryModel]
    var products: [ProductModel]
    
    init(data: [String: Any], parentCategory: ProductCategoryModel?, depth: Int) {
        self.id = UIView.toInt(data["term_id"])
        self.name = data["name"] as? String ?? ""
        self.slug = data["slug"] as? String ?? ""
        self.order = UIView.toInt(data["term_order"])
        self.count = UIView.toInt(data["count"])
        self.depth = depth
        self.language = LangEnum(code: data["dil_kodu"] as? String ?? "")
        self.parentCategory = parentCategory
        self.children = [ProductCategoryModel]()
        self.products = [ProductModel]()
    }

    func appendChild(_ child: ProductCategoryModel) {
        self.children.append(child);
    }
    
    func appendProduct(_ product: ProductModel) {
        self.products.append(product);
    }
    
}
