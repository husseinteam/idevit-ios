//
//  ProductModel.swift
//  İdevit Seramik Ürün Kataloğu
//
//  Created by Hüseyin Sönmez on 06/02/2017.
//  Copyright © 2017 Hüseyin Sönmez. All rights reserved.
//

import Foundation
import UIKit

class ProductModel {
    
    func fetchMainImage(_ callback: @escaping (UIImage?, [UIImage?]) -> Void) {
        if let url = self.mainImageUrl {
            url.fetchImage { mainImage in
                var featureds = [UIImage?]()
                for pictureMeta in self.pictureMetaList {
                    if let foundFrame =
                        AppModel.assets?.pictures.filter({ $0.name == pictureMeta["meta_key"] as? String ?? ""}).first {
                        foundFrame.fetchFrame({ (frame) in
                            featureds.append(frame)
                            if featureds.count == self.pictureMetaList.count {
                                callback(mainImage, featureds)
                            }
                        })
                    }
                }
                if self.pictureMetaList.count == 0 {
                    callback(mainImage, [UIImage?]())
                }
            }
        } else {
            callback(#imageLiteral(resourceName: "no-image"), [UIImage?]())
        }
    }
    var featureImages: [UIImage]!
    var materials: [MaterialModel]!
    let productCode: String!
    let menuOrder: Int!
    let productSerial: String!
    let productDescription: String!
    let productDescription2: String!
    let tags: [String]!
    let mainImageUrl: URL?
    
    private let pictureMetaList: [[String: Any]]!
    
    init(data: [String: Any]) {
        var serial = ""
        var description = ""
        var description2 = ""
        if let metas = data["metalar"] as? [[String: Any]] {
            if let meta1 = metas.filter ({ $0["meta_key"] as? String ?? "" == "urunkodu" }).first {
                serial = meta1["meta_value"] as? String ?? ""
            }
            if let meta2 = metas.filter ({ $0["meta_key"] as? String ?? "" == "urunaciklamasi_0_urunkisaaciklamasi" }).first {
                description = meta2["meta_value"] as? String ?? ""
            }
            if let meta3 = metas.filter ({ $0["meta_key"] as? String ?? "" == "urunaciklamasi_1_urunkisaaciklamasi" }).first {
                description2 = meta3["meta_value"] as? String ?? ""
            }
        }
        self.productCode = data["post_title"] as? String ?? ""
        self.productSerial = serial
        self.productDescription = description
        self.productDescription2 = description2
        self.menuOrder = UIView.toInt(data["menu_order"])
        self.mainImageUrl = URL(string: data["resimurl"] as? String ?? "")
        self.featureImages = [UIImage]()
        self.materials = [MaterialModel]()
        self.tags = [String]()
        self.pictureMetaList = (data["metalar"] as? [[String: Any]])?
            .uniquefied(by: { $0["meta_key"] as? String ?? "" })
            .filter({ m in
                let key = m["meta_key"] as? String ?? ""
                let value = m["meta_value"] as? String ?? ""
                return key != "yeni_urun_iconu_ekle" && value == "1" &&
                    AppModel.assets?.pictures.any({ $0.name == key }) ?? false
            })
        if let newProductData = (data["metalar"] as? [[String: Any]])?.first(of: {
            $0["meta_key"] as? String ?? "" == "yeni_urun_iconu_ekle" &&
                $0["meta_value"] as? String ?? "" == "1" }) {
            self.pictureMetaList.append(newProductData)
        }
        for tag in data["etiketler"] as? [String] ?? [String]() {
            tags.append(tag)
        }
    }
    
    func addingMaterial(material: MaterialModel) -> ProductModel {
        self.materials.append(material)
        return self
    }
    
}
