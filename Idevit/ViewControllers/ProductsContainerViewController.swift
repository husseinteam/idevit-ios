//
//  ProductsContainerViewController.swift
//  Idevit
//
//  Created by Hüseyin Sönmez on 14/02/2017.
//  Copyright © 2017 Hüseyin Sönmez. All rights reserved.
//

import UIKit

class ProductsContainerViewController: SlideMenuController, SlideMenuControllerDelegate {
    
    override func awakeFromNib() {
        self.prepareSlideMenuControllers()
        super.awakeFromNib()
    }
 
    @IBAction func slideTriggerTapped(_ sender: UIBarButtonItem) {
        self.toggleRight()
    }
    
    @IBAction func seriesTriggerTapped(_ sender: UIBarButtonItem) {
        self.toggleLeft()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.forceNav()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
    }
    
    private var productsMenuViewController: ProductsMenuViewController? = nil
    private var seriesMenuViewController: SeriesMenuViewController? = nil
    
    private func prepareSlideMenuControllers() {
        if let prodVC = self.storyboard?.instantiateViewController(
            withIdentifier: "Products") as? ProductsViewController,
            let seriesVC = self.storyboard?.instantiateViewController(
                withIdentifier: "SeriesMenu") as? SeriesMenuViewController,
            let menuVC = self.storyboard?.instantiateViewController(
                withIdentifier: "ProductsMenu") as? ProductsMenuViewController {
            self.rightViewController = menuVC
            self.leftViewController = seriesVC
            self.mainViewController = prodVC
            menuVC.delegate = prodVC
            seriesVC.delegate = prodVC
            prodVC.productTreeParsedHandler = { idevit in
                prodVC.setBreadcrumbsText(idevit.rootCategories.first!.name!)
                menuVC.segmentTrack = SegmentTrack(tree: idevit)
                menuVC.prepareTreeView()
                seriesVC.segmentTrack = SegmentTrack(tree: idevit)
                seriesVC.prepareTreeView()
            }
            menuVC.menuInfoTappedHandler = { menu, curTrack in
                prodVC.setCollectionData(curTrack.products)
                prodVC.setBreadcrumbsText(menu.segmentTrack?.currentBreadCrumbsText())
            }
            menuVC.menuCellTappedHandler = { menu, curItem in
                prodVC.setBreadcrumbsText(menu.segmentTrack?.currentBreadCrumbsText())
                if curItem.children.count == 0 {
                    seriesVC.closeLeft()
                    seriesVC.close()
                    menu.closeRight()
                    menu.close()
                }
            }
            seriesVC.menuInfoTappedHandler = { menu, curTag in
                prodVC.setCollectionData(curTag.products)
                prodVC.setBreadcrumbsText(menu.segmentTrack?.currentBreadCrumbsText(forTag: true))
            }
            seriesVC.menuCellTappedHandler = { menu, curItem in
                prodVC.setBreadcrumbsText(menu.segmentTrack?.currentBreadCrumbsText(forTag: true))
                if curItem.children.count == 0 {
                    menuVC.closeRight()
                    menuVC.close()
                    menu.closeLeft()
                    menu.close()
                }
            }
            self.productsMenuViewController = menuVC
            self.seriesMenuViewController = seriesVC
        }
    }
    
    override func closeRight() {
        super.closeRight()
        self.productsMenuViewController?.close()
    }
    
    override func closeLeft() {
        super.closeLeft()
        self.seriesMenuViewController?.close()
    }
    
    override func openRight() {
        super.openRight()
        self.seriesMenuViewController?.closeLeft()
    }
    
    override func openLeft() {
        super.openLeft()
        self.productsMenuViewController?.closeRight()
    }
}
