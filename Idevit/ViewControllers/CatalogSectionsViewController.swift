//
//  PdfViewController.swift
//  Idevit
//
//  Created by Lampiclobe on 20/02/2017.
//  Copyright © 2017 Hüseyin Sönmez. All rights reserved.
//

import UIKit

class CatalogSectionsViewController: UIViewController, SeguedViewControllerProtocol {
    
    // MARK: - Outlets
    @IBOutlet weak var CatalogView: UITableView!
    @IBOutlet weak var activator: UIActivityIndicatorView!
    @IBOutlet weak var pdfLoading: UILabel!
    @IBOutlet weak var menu: UIBarButtonItem!
    
    // MARK: - Actions
    @IBAction func menuTriggerButtonTapped(_ sender: UIBarButtonItem) {
        if self.isAnimating == false {
            self.toggleQuarterAnimation(sender)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.prepareCatalogView()
        self.loadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.forceNav()
        if self.onScreen == false {
            UIView.animate(withDuration: TimeInterval(1), delay: TimeInterval(0.1), options: .curveEaseInOut, animations: {
                self.quarterTranslation(counterClockWise: true)
                self.menu.image = #imageLiteral(resourceName: "menu-horizontal")
            }) { done in
                self.menu.tag = 2
            }
        }
    }
    
    // MARK: - SeguedViewControllerProtocol
    var destinationIdentifier: String {
        get {
            return "displayPage"
        }
    }
    
    // MARK: - Segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == destinationIdentifier {
            if let destVC = segue.destination as? PdfPageViewController {
                destVC.setupCatalog(data: self.document!.0, page: self.document!.1)
            }
        }
    }
    
    
    // MARK: - Data
    var data: [PdfSectionModel] = [PdfSectionModel]()
    private var document: (Data, Int)? = nil {
        didSet {
            if self.document != nil {
                if self.document!.1 != 1 {
                    self.performSegue(withIdentifier: destinationIdentifier, sender: self)
                }
            }
        }
    }
    
    // MARK: - Locals
    private var incrementIndex: Int = 0
    var isAnimating: Bool = false
    var onScreen: Bool{
        get {
            return self.CatalogView.layer.opacity == 1.0
        }
    }
    
    // MARK: - Methods
    private func loadData() {
        if let idevit = AppModel.idevit {
            self.data = idevit.sections
            self.CatalogView.reloadData()
        }
    }
    private func prepareCatalogView() {
        self.CatalogView.backgroundView = nil
        self.CatalogView.backgroundColor = UIColor(colorLiteralRed: 66/256, green: 63/256, blue: 82/256, alpha: 1.0)
        self.CatalogView.register(UINib(nibName: String(describing: PriceCatalogCell.self), bundle: nil),
                                  forCellReuseIdentifier: String(describing: PriceCatalogCell.self))
    }
    private func loadFirstPage() {
        self.loadPdf(at: 1)
    }
    private func toggleQuarterAnimation(_ sender: UIBarButtonItem) {
        if sender.tag == 1 {
            self.loadFirstPage()
        }
        self.isAnimating = true
        UIView.animate(withDuration: TimeInterval(1), delay: TimeInterval(0.1), options: .curveEaseInOut, animations: {
            self.quarterTranslation(counterClockWise: sender.tag == 1)
            sender.image = sender.tag == 2 ? #imageLiteral(resourceName: "menu-vertical") : #imageLiteral(resourceName: "menu-horizontal")
        }) { done in
            sender.tag = sender.tag == 1 ? 2 : 1
            self.isAnimating = false
        }
    }
    
    private func quarterTranslation(counterClockWise: Bool) {
        let a = counterClockWise ? CGFloat(M_PI_2) : -CGFloat(M_PI_2)
        let x = -self.CatalogView.center.x
        let y = -self.CatalogView.center.y
        
        self.CatalogView.transform = self.CatalogView.transform
            .translatedBy(x: x,y: y)
            .rotated(by: a)
            .translatedBy(x: -x,y: -y);
        
        if (counterClockWise) {
            self.CatalogView.layer.opacity = 1.0
            self.view.sendSubview(toBack: self.CatalogView)
        } else {
            self.CatalogView.layer.opacity = 0.0
            self.view.bringSubview(toFront: self.CatalogView)
        }
    }
    
    func loadPdf(at page: Int) {
        if self.activator.isAnimating == false {
            if let pdfUrl = AppModel.assets?.catalogPdfUrl {
                self.activator.startAnimating()
                self.pdfLoading.isHidden = false
                pdfUrl.prefetchData(named: "idevit-katalog", withExtension: "pdf") { pdfdata in
                    self.document = (pdfdata, page)
                    self.activator.stopAnimating()
                    self.pdfLoading.isHidden = true
                }
            }
        }
    }
    
}

extension CatalogSectionsViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.CatalogView.dequeueReusableCell(withIdentifier: String(describing: PriceCatalogCell.self))
        guard let priceCell = cell as? PriceCatalogCell else {
            return cell!
        }
        priceCell.model = self.data[indexPath.row]
        priceCell.selectedHandler = { cell, selected in
            if selected {
                self.loadPdf(at: self.data[indexPath.row].page)
            }
        }
        let clearView = UIView()
        clearView.backgroundColor = .clear
        priceCell.selectedBackgroundView = clearView
        return priceCell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = .clear
    }
    
}


