//
//  ContactsTableViewController.swift
//  Idevit
//
//  Created by Lampiclobe on 27/02/2017.
//  Copyright © 2017 Hüseyin Sönmez. All rights reserved.
//

import UIKit

class ContactsTableViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    // MARK: - Outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activator: UIActivityIndicatorView!
    @IBOutlet weak var idevitLink: UILabel!
    @IBOutlet weak var fb: UIImageView!
    @IBOutlet weak var twitter: UIImageView!
    @IBOutlet weak var gplus: UIImageView!
    
    // MARK: - Data
    var data: [ContactModel] = [ContactModel]() {
        didSet {
            if self.data.count > 0 {
                self.tableView.reloadData()
                self.activator.stopAnimating()
            }
        }
    }
    
    // MARK: Gestures
    func idevitLinkTapped(_ sender: UITapGestureRecognizer) {
        URL(string: "http://idevit.com.tr")?.tryOpen()
    }
    
    func fbLinkTapped(_ sender: UITapGestureRecognizer) {
        URL(string: "http://idevit.com.tr")?.tryOpen()
    }
    
    func twitterLinkTapped(_ sender: UITapGestureRecognizer) {
        URL(string: "http://idevit.com.tr")?.tryOpen()
    }
    
    func gplusLinkTapped(_ sender: UITapGestureRecognizer) {
        URL(string: "http://idevit.com.tr")?.tryOpen()
    }
    
    // MARK: Methods
    private func loadGestures() {
        let linkTap = UITapGestureRecognizer(target: self, action: #selector(self.idevitLinkTapped))
        self.idevitLink?.isUserInteractionEnabled = true
        self.idevitLink?.addGestureRecognizer(linkTap)
        
        let fbTap = UITapGestureRecognizer(target: self, action: #selector(self.fbLinkTapped))
        self.fb?.isUserInteractionEnabled = true
        self.fb?.addGestureRecognizer(fbTap)
        
        let twitterTap = UITapGestureRecognizer(target: self, action: #selector(self.twitterLinkTapped))
        self.twitter?.isUserInteractionEnabled = true
        self.twitter?.addGestureRecognizer(twitterTap)
        
        let gplusTap = UITapGestureRecognizer(target: self, action: #selector(self.gplusLinkTapped))
        self.gplus?.isUserInteractionEnabled = true
        self.gplus?.addGestureRecognizer(gplusTap)
    }
    
    private func loadData() {
        self.activator.startAnimating()
        AppModel.ideveng.parseContacts { (contacts) in
            self.data = contacts
        }
    }
    
    private func loadTableView() {
        self.tableView.register(UINib(nibName: String(describing: ContactItemCell.self), bundle: nil),
                                forCellReuseIdentifier: String(describing: ContactItemCell.self))
        self.tableView.register(UINib(nibName: String(describing: MapItemCell.self), bundle: nil),
                                forCellReuseIdentifier: String(describing: MapItemCell.self))
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadTableView()
        self.loadData()

    }

    // MARK: - Table view data source

    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.data.count
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let m = self.data[indexPath.row]
        if m.isMap, let _ = tableView.dequeueReusableCell(withIdentifier: String(describing: MapItemCell.self)) as? MapItemCell {
            return 215
        } else if let _ = tableView.dequeueReusableCell(withIdentifier: String(describing: ContactItemCell.self)) as? ContactItemCell {
            return 66
        } else {
            return 44
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let m = self.data[indexPath.row]
        if m.isMap, let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: MapItemCell.self)) as? MapItemCell {
            cell.model = m
            return cell
        } else if let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ContactItemCell.self)) as? ContactItemCell {
            cell.model = m
            return cell
        } else {
            return UITableViewCell()
        }
    }
}
