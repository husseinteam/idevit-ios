//
//  PdfViewController.swift
//  Idevit
//
//  Created by Lampiclobe on 20/02/2017.
//  Copyright © 2017 Hüseyin Sönmez. All rights reserved.
//

import UIKit

class CatalogPdfViewController: UIViewController {

    // MARK: - Outlets
    @IBOutlet weak var pdfContent: UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.drawPDF()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.forceNav()
    }
    
    // MARK: - Data
    private var document: (Data, Int, Int)? = nil

    // MARK: - Locals
    var page: Int! = 0
    var pageData: Data? = nil
    var password: String? = nil
    private var incrementIndex: Int = 0
    
    // MARK: - Methods
    func setup(data: Data, page: Int, password: String? = nil) {
        self.page = page
        self.pageData = data
        self.password = password
    }
    
    func curlPage(to index: Int) {
        self.incrementIndex = index
    }
    
    private func drawPDF() {
        guard let pageData = self.pageData else {
            return
        }
        guard let dataProvider = CGDataProvider(data: pageData as CFData) else {
            return
        }
        guard let document = CGPDFDocument(dataProvider) else {
            return
        }
        if let password = self.password,
            let cPasswordString = password.cString(using: .utf8) {
            if document.isEncrypted && !document.unlockWithPassword("") {
                if !document.unlockWithPassword(cPasswordString) {
                    print("CGPDFDocumentCreateX: Unable to unlock")
                }
            }
        }
        
        guard let docpage = document.page(at: self.page + self.incrementIndex) else { return }
        let pageRect = docpage.getBoxRect(.mediaBox)
        var img: UIImage? = nil
        if #available(iOS 10.0, *) {
            let renderer = UIGraphicsImageRenderer(size: pageRect.size)
            img = renderer.image { ctx in
                UIColor.white.set()
                ctx.fill(pageRect)
                ctx.cgContext.translateBy(x: 0.0, y: pageRect.size.height)
                ctx.cgContext.scaleBy(x: 1.0, y: -1.0)
                ctx.cgContext.drawPDFPage(docpage)
            }
        } else {
            UIGraphicsBeginImageContext(pageRect.size)
            let ctx = UIGraphicsGetCurrentContext()
            UIColor.white.set()
            ctx?.fill(pageRect)
            ctx?.translateBy(x: 0.0, y: pageRect.size.height)
            ctx?.scaleBy(x: 1.0, y: -1.0)
            ctx?.drawPDFPage(docpage)
            img = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext()
        }
        
        self.pdfContent?.applyImage(img)
    }
}
