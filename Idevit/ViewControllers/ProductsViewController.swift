//
//  ProductsCollectionViewController.swift
//  İdevit Seramik Ürün Kataloğu
//
//  Created by Hüseyin Sönmez on 06/02/2017.
//  Copyright © 2017 Hüseyin Sönmez. All rights reserved.
//

import UIKit

private let reuseIdentifier = "ProductCell"
private let menuReuseIdentifier = "MenuItemCell"


private let groupCount = UI_USER_INTERFACE_IDIOM() == .pad ? 20 : 6

class ProductsViewController: UIViewController, UISearchBarDelegate, ProductViewControllerProtocol, SeguedViewControllerProtocol {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView.register(UINib(nibName: reuseIdentifier, bundle:nil),
                                     forCellWithReuseIdentifier: reuseIdentifier)
        self.initTree()
        self.addDoneButtonOnKeyboard()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.search.text = ""
        self.forceNav()
    }
    
    // MARK: - Outlets
    @IBOutlet weak var activator: UIActivityIndicatorView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var breadcrumbs: UILabel!
    @IBOutlet weak var search: UISearchBar!
    
    // MARK: - Locals
    var productTree = IdevitModel()
    var productTreeParsedHandler: ((IdevitModel) -> Void)? = nil
    var selectedCell: ProductCell? = nil {
        didSet {
            if self.selectedCell != nil {
                self.performSegue(withIdentifier: destinationIdentifier, sender: self)
            }
        }
    }
    var cellCounter = 0
    var nextGroup = groupCount
    var filterText: String! = "" {
        didSet {
            if self.filterText.isEmpty {
                self.elements = self.data.sliced(from: 0, to: self.nextGroup)
            } else {
                self.elements = self.data.filter {
                    $0.productCode.contains(self.filterText) ||
                        $0.tags.any { t in t.contains(self.filterText) }
                }
            }
            self.reload()
        }
    }
    var elements = [ProductModel]() {
        didSet {
            self.reload()
        }
    }
    var data = [ProductModel]()
    
    // MARK: ProductViewControllerProtocol
    func apply(callback: @escaping () -> Void) {
        callback()
    }
    
    // MARK: Segue
    var destinationIdentifier: String {
        get {
            return "productDetail"
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destVC = segue.destination as? ProductDetailViewController {
            destVC.model = self.selectedCell
        }
    }
    // MARK: - Search
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.filterText = searchText
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.setValue("Kapat", forKey: "_cancelButtonText")
    }
    
    // Keypad First Respond
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle       = UIBarStyle.default
        let flexSpace              = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem  = UIBarButtonItem(title: "Kapat", style: .done, target: self, action: #selector(self.doneButtonAction(_:)))
        
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        self.search.inputAccessoryView = doneToolbar
        
    }
    
    func doneButtonAction(_ sender: UIBarButtonItem)
    {
        self.search.resignFirstResponder()
    }
    
    // MARK: Methods
    private func initTree() {
        self.activator.startAnimating()
        if let idevit = AppModel.idevit {
            self.productTree = idevit
            self.data = self.productTree.rootCategories[0].products
            self.elements = self.data.sliced(from: 0, to: groupCount)
            self.activator.stopAnimating()
            self.productTreeParsedHandler?(idevit)
            self.prepareSearch()
        }
    }
    
    private func prepareSearch() {
        self.definesPresentationContext = true
        self.search.placeholder = "Arama Yapın"
        self.search.delegate = self
    }
    
    func reload() {
        DispatchQueue.main.async {
            self.collectionView.reloadData()
        }
    }
    
    func loadNextGroup() {
        if self.nextGroup <= self.cellCounter + groupCount {
            self.elements.append(
                contentsOf: self.data.filter({
                    self.filterText.isEmpty || $0.productCode.contains(self.filterText) ||
                        $0.tags.any { t in t.contains(self.filterText) }
                }).sliced(from: self.nextGroup,
                          to: self.cellCounter + groupCount)
            )
            self.nextGroup += groupCount
            self.reload()
        }
    }
    
    func setCollectionData(_ data: [ProductModel]) {
        self.data = data
        self.elements = self.data.sliced(from: 0, to: self.cellCounter + groupCount)
        if self.elements.count > 0 {
            self.collectionView.setContentOffset(CGPoint.zero, animated: true)
        }
    }
    
    func setBreadcrumbsText(_ text: String!) {
        self.breadcrumbs?.isHidden = false
        self.breadcrumbs?.text = text
    }

}

extension ProductsViewController : UICollectionViewDataSource, UICollectionViewDelegate {

    // MARK: UICollectionViewDataSource

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.elements.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath)
        if let productCell = cell as? ProductCell {
            productCell.model = self.elements[indexPath.item]
            productCell.handler = { (product) in
                self.selectedCell = productCell
            }
            return productCell
        } else {
            return cell
        }
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        self.cellCounter = indexPath.item + 1
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        
        if maximumOffset - currentOffset < 0 {
            self.loadNextGroup()
        }
    }
    // MARK: UICollectionViewDelegate
}
