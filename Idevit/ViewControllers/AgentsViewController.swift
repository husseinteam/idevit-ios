//
//  AgentsViewController.swift
//  Idevit
//
//  Created by Lampiclobe on 21/02/2017.
//  Copyright © 2017 Hüseyin Sönmez. All rights reserved.
//

import UIKit

class AgentsViewController: UICollectionViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        self.clearsSelectionOnViewWillAppear = false

        self.collectionView!.register(UINib(nibName: String(describing: AgentItemCell.self), bundle: nil),
                                     forCellWithReuseIdentifier: String(describing: AgentItemCell.self))
        AppModel.ideveng.parseAgents { (agents) in
            self.data = agents
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Outlets
    @IBOutlet weak var activator: UIActivityIndicatorView!
    
    // MARK: - Vars
    var data: [AgentModel] = [AgentModel]() {
        didSet {
            if self.data.count > 0 {
                DispatchQueue.main.async {
                    self.collectionView?.reloadData()
                    self.activator.stopAnimating()
                }
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return self.data.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(
            withReuseIdentifier: String(describing: AgentItemCell.self),
            for: indexPath) as? AgentItemCell else {
                return UICollectionViewCell()
        }
    
        cell.model = self.data[indexPath.item]
        return cell
    }
    
    // MARK: UICollectionViewDelegate


}
