//
//  ProductDetailViewController.swift
//  Idevit
//
//  Created by Lampiclobe on 21/02/2017.
//  Copyright © 2017 Hüseyin Sönmez. All rights reserved.
//

import UIKit

class ProductDetailViewController: UIViewController, SeguedViewControllerProtocol {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        DispatchQueue.main.async {
            self.navigationController?.setNavigationBarHidden(false, animated: false)
            self.navigationController?.navigationItem.backBarButtonItem?.tintColor = .white
            self.prepareOutlets()
        }
    }
    
    //MARK: - Outlets
    @IBOutlet weak var modelImage: UIImageView!
    @IBOutlet weak var modelName: UILabel!
    @IBOutlet weak var modelSerial: UILabel!
    @IBOutlet weak var modelDescription: UILabel!
    @IBOutlet weak var modelDescriptor2: UILabel!
    @IBOutlet weak var activator: UIActivityIndicatorView!
    @IBOutlet var featuredImages: [UIImageView]!
    
    // MARK: Vars
    var model: ProductCell? = nil
    
    //MARK: - Actions
    @IBAction func backTapped(_ sender: UIBarButtonItem) {
        let _ = self.navigationController?.popViewController(
            animated: true)
    }
    
    //MARK: - Methods
    private func prepareOutlets() {
        if let pmodel = self.model {
            self.activator.startAnimating()
            let img = pmodel.mainImageView.image
            let featureds = pmodel.featureImages.flatMap({ $0.image })
            self.modelImage?.setImageFading(img) {
                self.navigationController?.navigationBar.topItem?.title =
                    pmodel.model.productCode!
                self.modelName?.text = pmodel.model.productCode!
                self.modelSerial?.text = pmodel.model.productSerial
                self.modelDescription?.text =
                    pmodel.model.productDescription
                self.modelDescriptor2?.text =
                    pmodel.model.productDescription2
                let limit = min(self.featuredImages.count,
                                featureds.count)
                for i in 0 ..< limit {
                    self.featuredImages[i].setImageFading(featureds[i])
                }
                self.activator.stopAnimating()
            }
        }
    }
    
    // MARK: - SeguedViewControllerProtocol
    var destinationIdentifier: String {
        get {
            return "backToProducts"
        }
    }
    
}
