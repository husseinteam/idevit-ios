//
//  MainTabsViewController.swift
//  İdevit Seramik Ürün Kataloğu
//
//  Created by Hüseyin Sönmez on 06/02/2017.
//  Copyright © 2017 Hüseyin Sönmez. All rights reserved.
//

import UIKit

class MainTabsViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.checkAndPrepare()
    }
    
    // MARK: - Methods
    private func checkAndPrepare() {
        if Reachability.isConnectedToNetwork() == false {
            AppModel.ideveng.noConnectionHandler = { err in
                let alert = UIAlertController(title: "Hata", message: "İnternete Bağlı Değilsiniz. Lütfen Bağlantınızı Kontrol Ediniz.", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Tamam", style: .default, handler:  {(a) in
                    exit(0)
                }))
                
                // show the alert
                self.present(alert, animated: true, completion: nil)
            }
        }
        self.prepareIdevit()
    }
    private func animateLoading(handler: (@escaping ()-> Void) -> Void) {
        let activator =
            UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
        self.view.addSubview(activator)
        activator.color = .black
        activator.hidesWhenStopped = true
        activator.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        activator.translatesAutoresizingMaskIntoConstraints = false
        let horizontalConstraint = NSLayoutConstraint(item: activator, attribute: .centerX, relatedBy: .equal, toItem: self.view, attribute: .centerX, multiplier: 1.0, constant: 0.0)
        let verticalConstraint = NSLayoutConstraint(item: activator, attribute: .centerY, relatedBy: .equal, toItem: self.view, attribute: .centerY, multiplier: 1.0, constant: 0.0)
        NSLayoutConstraint.activate([horizontalConstraint, verticalConstraint])
        DispatchQueue.main.async {
            activator.startAnimating()
        }
        handler {
            DispatchQueue.main.async {
                self.tabBar.tintColor = .white
                self.tabBar.isHidden = false
                activator.stopAnimating()
            }
        }
    }
    private func prepareIdevit() {
        self.tabBar.isHidden = true
        self.view.backgroundColor = .white
        self.animateLoading { (next) in
            AppModel.ideveng.parseAssets { assets in
                AppModel.assets = assets
                if let catalogLogoURL = assets.catalogLogoUrl {
                    catalogLogoURL.prefetchData(named: "catalog-logo", withExtension: "png") { logoData in
                        DispatchQueue.main.async {
                            self.viewIfLoaded?.addBackground(image: UIImage(data: logoData) ?? #imageLiteral(resourceName: "home-bg"), contextMode: .scaleAspectFill)
                        }
                        AppModel.ideveng.parseProductTree(callback: { (idevit) in
                            AppModel.set(idevit: idevit)
                            next()
                        })
                    }
                }
            }
        }
    }
    // UITabBarDelegate
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        if let productsVC = self.viewControllers?.filter({
            ($0 as? UINavigationController)?.viewControllers.first is ProductsContainerViewController
        }).first {
            if item.tag == 571 {
                productsVC.openRight()
                item.tag = 1453
            } else if item.tag == 1453 {
                productsVC.closeRight()
                item.tag = 571
            }
        }
    }
    
    override var prefersStatusBarHidden: Bool {
        get {
            return false
        }
    }
    
}
