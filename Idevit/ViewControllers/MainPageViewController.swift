//
//  MainPageViewController.swift
//  Idevit
//
//  Created by Lampiclobe on 22/02/2017.
//  Copyright © 2017 Hüseyin Sönmez. All rights reserved.
//

import UIKit

class MainPageViewController: UIViewController, SeguedViewControllerProtocol {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBOutlet weak var activator: UIActivityIndicatorView!
    
    // MARK: - Data
    private var document: (Data, Int)? = nil {
        didSet {
            if self.document != nil {
                self.performSegue(withIdentifier: destinationIdentifier, sender: self)
            }
        }
    }
    
    // MARK: - Methods
    func loadPdf(at page: Int) {
        if self.activator.isAnimating == false {
            self.activator.startAnimating()
            AppModel.ideveng.parseOnePagePdf { pdf in
                if let pdfData = pdf {
                    self.document = (pdfData, page)
                }
            }
        }
    }

    
    // MARK: Segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destVC = segue.destination as? PdfPageViewController {
            destVC.setupCatalog(data: self.document!.0, page: self.document!.1)
            self.activator.stopAnimating()
        }
    }
    var destinationIdentifier: String {
        get {
            return "displayImageCatalog"
        }
    }
    
    // MARK: - Actions
    @IBAction func contactsTapped(_ sender: UIButton) {
        if AppModel.idevit != nil {
            self.loadPdf(at: 1)
        }
    }
    @IBAction func priceCatalogTapped(_ sender: UIButton) {
        if AppModel.idevit != nil {
            self.tabBarController?.selectedIndex = 1
        }
    }
    @IBAction func productsTapped(_ sender: UIButton) {
        if AppModel.idevit != nil {
            self.tabBarController?.selectedIndex = 2
        }
    }
}
