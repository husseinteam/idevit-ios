//
//  SeriesMenuViewController.swift
//  Idevit
//
//  Created by Lampiclobe on 02/03/2017.
//  Copyright © 2017 Hüseyin Sönmez. All rights reserved.
//

import UIKit

class SeriesMenuViewController : UIViewController, RATreeViewDataSource, RATreeViewDelegate, UISearchBarDelegate {
    
    convenience init() {
        self.init(nibName : nil, bundle: nil)
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    @IBOutlet weak var activator: UIActivityIndicatorView!

    //MARK: - Vars
    var delegate: ProductViewControllerProtocol? = nil
    var segmentTrack: SegmentTrack? = nil
    var menuInfoTappedHandler : ((SeriesMenuViewController, ProductTagModel) -> Void)?
    var menuCellTappedHandler : ((SeriesMenuViewController, ProductTagModel) -> Void)?
    private var searchController: UISearchController? = nil
    private var treeView: RATreeView!

    // MARK: - Methods
    func prepareTreeView() {
        DispatchQueue.main.async {
            self.treeView = RATreeView()
            self.treeView.frame = self.view.bounds
            self.treeView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            self.treeView.delegate = self
            self.treeView.dataSource = self
            self.treeView.register(UINib(nibName: String(describing: ProductMenuCell.self), bundle: nil),
                                   forCellReuseIdentifier: String(describing: ProductMenuCell.self))
            self.treeView.backgroundColor = .white
            self.treeView.layer.opacity = 0.8
            self.view.addSubview(self.treeView)
            
            self.segmentTrack?.set(current: (self.segmentTrack?.rootTags()[0])!)
            self.reload()
            self.activator.stopAnimating()
        }
    }
    
    func close() {
        self.delegate?.apply {
            if let prodTabItem = self.tabBarController?.tabBar.items?.filter({ $0.tag == 571 || $0.tag == 1453 }).first {
                prodTabItem.tag = 571
            }
            if let curtag = self.segmentTrack?.curtag {
                self.menuInfoTappedHandler?(self, curtag)
            }
        }
    }
    
    private func reload() {
        self.treeView.reloadData()
    }
    
    // MARK: - TreeView
    func treeView(_ treeView: RATreeView, numberOfChildrenOfItem item: Any?) -> Int {
        if let item = item as? ProductTagModel {
            return item.children.count
        } else {
            return self.segmentTrack?.rootTags().count ?? 0
        }
    }
    
    func treeView(_ treeView: RATreeView, child index: Int, ofItem item: Any?) -> Any {
        if let item = item as? ProductTagModel {
            return item.children[index]
        } else {
            return (self.segmentTrack?.rootTags()[index])!
        }
    }
    
    func treeView(_ treeView: RATreeView, cellForItem item: Any?) -> UITableViewCell {
        let cell = treeView.dequeueReusableCell(withIdentifier: String(describing: ProductMenuCell.self)) as! ProductMenuCell
        guard let item = item as? ProductTagModel else {
            return cell
        }
        cell.tagmodel = item
        cell.infoTappedHandler = { [weak treeView] cell in
            guard let treeView = treeView else {
                return;
            }
            self.slideMenuController()?.closeLeft()
            self.close()
            let category = treeView.item(for: cell) as! ProductTagModel
            self.segmentTrack?.set(current: category)
            self.menuInfoTappedHandler?(self, category)
        }
        return cell
    }
    
    func treeView(_ treeView: RATreeView, heightForRowForItem item: Any) -> CGFloat {
        return 50
    }
    
    func treeView(_ treeView: RATreeView, canEditRowForItem item: Any) -> Bool {
        return false
    }
    
    func treeView(_ treeView: RATreeView, didExpandRowForItem item: Any) {
        guard let cell = treeView.cell(forItem: item) as? ProductMenuCell else {
            return
        }
        guard let item = item as? ProductTagModel else {
            return
        }
        self.segmentTrack?.set(current: item)
        cell.tagmodel = item
        self.menuCellTappedHandler?(self, item)
    }
    
    // MARK: - SlideMenuControllerDelegate
    func leftDidClose() {
        if let prodTabItem = self.tabBarController?.tabBar.items?.filter({ $0.tag == 571 || $0.tag == 1453 }).first {
            prodTabItem.tag = 571
        }
        if let curtag = self.segmentTrack?.curtag {
            self.menuInfoTappedHandler?(self, curtag)
        }
    }
    
}
