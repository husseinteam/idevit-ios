//
//  PdfContentViewController.swift
//  Idevit
//
//  Created by Lampiclobe on 20/02/2017.
//  Copyright © 2017 Hüseyin Sönmez. All rights reserved.
//

import UIKit

class PdfPageViewController: UIPageViewController, UIPageViewControllerDataSource {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.dataSource = self
        self.setViewControllers([self.getViewController(at: self.distanceIndex)] as [UIViewController],
                                direction: .forward,
                                animated: false,
                                completion: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.forceNav()
    }
    
    // MARK: Methods
    func setupCatalog(data: Data, page: Int, password: String? = nil) {
        self.setupHandle = { controller in
            controller.setup(data: data, page: page, password: password)
        }
    }
    
    //MARK: - Actions
    
    @IBAction func backTapped(_ sender: UIBarButtonItem) {
        
        let _ = self.navigationController?.popViewController(animated: true)
        
    }
    
    // MARK: -Vars
    private var distanceIndex: Int = 0
    
    private var setupHandle: ((CatalogPdfViewController) -> Void)? = nil
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        self.distanceIndex += 1
        return getViewController(at: self.distanceIndex)
    }
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        self.distanceIndex -= 1
        return getViewController(at: self.distanceIndex)
    }
    func getViewController(at index: Int) -> CatalogPdfViewController {
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "CatalogPdfViewController") as! CatalogPdfViewController
        self.setupHandle?(controller)
        controller.curlPage(to: index)
        return controller
    }
    
}

